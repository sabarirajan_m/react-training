import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import NavBar from './components/NavBar';
import Clock from './components/Clock';
import Welcome from './components/Welcome';
import UserList from './components/UserList';
import CreateUser from './components/CreateUser';
import NotFound from './components/NotFound';
import Footer from './components/Footer';
import './App.css';

function App() {
    return (
        <Provider store={configureStore()}>
            <Router>
                <NavBar />
                <div className="main-content">
                    <Switch>
                        <Route exact path="/" component={Welcome} />
                        <Route path="/clock" component={Clock} />
                        <Route path="/create-user" component={CreateUser} />
                        <Route path="/user-list" component={UserList} />
                        <Route component={NotFound} />
                    </Switch>
                </div>
                <Footer />
            </Router>
        </Provider>
    );
}

export default App;
