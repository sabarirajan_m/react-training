/**
 * USER REDUCER
 */
import { CREATE_USER, DELETE_USER, SET_USER_LIST } from "../types";

const initialState = {
    users: []
};

function userReducer(state = initialState, action) {
    switch (action.type) {
        case CREATE_USER:
            const newUser = action.payload;
            return {
                users: [
                    ...state.users,
                    newUser
                ]
            };
        case DELETE_USER:
            const userId = action.payload;
            const newUserList = state.users.filter((user, index) => index !== userId);
            return {
                users: newUserList
            };
        case SET_USER_LIST:
            return {
                users: action.payload
            };
        default:
            return state;
    }
}

export default userReducer;
