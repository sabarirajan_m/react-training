import { CREATE_USER, DELETE_USER, SET_USER_LIST } from "../types";

/**
 * action to create new user
 * @param {object} user 
 */
export const createUser = user => dispatch => {
    console.log(JSON.stringify(user))
    dispatch({
        type: CREATE_USER,
        payload: user
    });
}

export const deleteUser = userId => dispatch => {
    dispatch({
        type: DELETE_USER,
        payload: userId
    });
}

export const getUserList = () => (dispatch) => {
    fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response => response.json())
        .then(json => {
            console.log("API response", json);
            // ignoring the API response and setting the sample value for demonstration purpose
            const users = mockAPIResponse();
            dispatch({
                type: SET_USER_LIST,
                payload: users
            });
        });
}

function mockAPIResponse() {
    const mockValues = [
        {"fname":"Sabari","lname":"Rajan","age":"21"}
    ];
    return mockValues;
}
