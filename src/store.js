import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';

const initialState = {};
const enhancers = [];

// add redux dev tools extension for development purpose
const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;
if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
}

// merge thunk middlware and redux dev tools
const composedEnhancers = compose(
    applyMiddleware(thunk),
    ...enhancers
);

export default function configureStore() {
    return createStore(
        rootReducer,
        initialState,
        composedEnhancers
    );
}
