import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteUser, getUserList } from '../../actions/userActions';
import './index.css';

class UserList extends React.Component {

    componentDidMount() {
        this.props.getUserList();
    }

    render() {
        const { users } = this.props;

        return (
            <>
                <h2>Users list</h2>

                {users.length > 0 ?
                    <table>
                        <thead>
                            <tr>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Age</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                users.map((user, index) => (
                                    <tr key={index}>
                                        <td>{user.fname}</td>
                                        <td>{user.lname}</td>
                                        <td>{user.age}</td>
                                        <td>
                                            <button
                                                type="button"
                                                onClick={() => this.props.deleteUser(index)}>
                                                Delete
                                        </button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    : "No users found"
                }
            </>
        )
    }
}

const mapStateToProps = state => ({
    users: state.userReducer.users
});

const mapDispatchToProps = dispatch => ({
    getUserList: () => dispatch(getUserList()),
    deleteUser: (userId) => dispatch(deleteUser(userId))
})

UserList.propTypes = {
    users: PropTypes.array.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
