import React from 'react';
import { NavLink } from 'react-router-dom';
import './index.css';

const Navbar = () => {
    return (
        <div>
            <ul>
                <li>
                <NavLink to="/" exact activeClassName="active">Home</NavLink>
                </li>
                <li>
                <NavLink to="/clock" activeClassName="active">Clock</NavLink>
                </li>
                <li>
                <NavLink to="/create-user" activeClassName="active">Create user</NavLink>
                </li>
                <li>
                <NavLink to="/user-list" activeClassName="active">User List</NavLink>
                </li>
            </ul>
      </div>
    );
}

export default Navbar;