import React from 'react';
import { connect } from 'react-redux';
import { createUser } from '../../actions/userActions';
import UserList from '../UserList';
import './index.css';

class CreateUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                fname: '',
                lname: '',
                age: '',
            }
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            user: {
                ...this.state.user,
                [name]: value
            },
        });
    };

    onSubmit = e => {
        e.preventDefault();
        this.props.createUser(this.state.user);
        // clear existing values
        this.setState({
            user: {
                fname: '',
                lname: '',
                age: '',
            }
        });
    };

    render() {
        const { fname, lname, age } = this.state.user;
        return (
            <div>
                <h2>Create User</h2>
                <div className="container">
                    <form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="col-25">
                                <label htmlFor="fname">First Name</label>
                            </div>
                            <div className="col-75">
                                <input
                                    type="text" id="fname" name="fname"
                                    value={fname}
                                    onChange={this.handleChange}
                                    placeholder="Your name.." />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-25">
                                <label htmlFor="lname">Last Name</label>
                            </div>
                            <div className="col-75">
                                <input
                                    type="text" id="lname" name="lname"
                                    value={lname}
                                    onChange={this.handleChange}
                                    placeholder="Your last name.." />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-25">
                                <label htmlFor="age">Age</label>
                            </div>
                            <div className="col-75">
                                <input type="text" id="age" name="age"
                                    value={age}
                                    onChange={this.handleChange}
                                    placeholder="Your age.." />
                            </div>
                        </div>
                        <div className="row">
                            <button type="submit">Submit</button>
                        </div>
                    </form>
                </div>
                <hr/>
                <UserList />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    createUser: (user) => dispatch(createUser(user))
})

export default connect(null, mapDispatchToProps)(CreateUser);
